package dk.semlerit.autocore.ws.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * 
 * @author edbbrpe
 * @version 1.0
 * @since 3.0.0
 */
@Entity
@Table(schema = "ACP", name = "PROPERTY")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Property implements Serializable {
	private static final long serialVersionUID = -49379898357096346L;

	@Id
	@Column(name = "KEYNAME", length = 100, insertable = true, updatable = false, nullable = false)
	private String key;
	@Column(name = "VALUE", length = 10000, insertable = true, updatable = true, nullable = false)
	private String value;
	@Column(name = "DESCRIPTION", length = 10000, insertable = true, updatable = true, nullable = false)
	private String description;

}
package dk.semlerit.autocore.ws.support;

import java.io.StringReader;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.xml.bind.DataBindingException;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Element;

import com.ibm.ws.webservices.engine.WebServicesFault;

import dk.semlerit.autocore.ws.client.model.xml.ErrorDataType;
import dk.semlerit.autocore.ws.model.ServiceError;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author edbbrpe
 * @version 1.0
 * @since 3.0.0
 */
@Slf4j
public abstract class FaultHandlerSupport {
	private static final Pattern ERROR_MESSAGE_PATTERN = Pattern.compile("([a-zA-Z0-9]+)\\=\\{([^}]*)\\}",
			Pattern.CASE_INSENSITIVE);
	private static final Pattern MESSAGE_REPLACE_PATTERN = Pattern.compile("\\{[^}]+\\}");
	private static final String METHOD_TRANSLATION_NOT_FOUND = "Ukendt fejl opstået - transaktionsId:'%s' - beskrivelse:'%s'";
	private static final ServiceCodeMessageBundle messages = new ServiceCodeMessageBundle("nl.messages", "service");

	/**
	 * 
	 * @param operation
	 * @param responseMsg
	 * @return
	 * @throws JAXBException
	 */
	public static ServiceError buildXMLServiceErrorTO(final String responseMsg) {
		final dk.semlerit.autocore.ws.client.model.xml.Message message = unmarshal(responseMsg,
				dk.semlerit.autocore.ws.client.model.xml.Message.class);
		final dk.semlerit.autocore.ws.client.model.xml.Message.Response response = message.getResponse();
		final dk.semlerit.autocore.ws.client.model.xml.ErrorType errorType = response.getError();

		/*
		 * Always prefer name from response if available, but account for legacy
		 * service implementations.
		 */
		final String serviceOperation = StringUtils.isEmpty(response.getName()) ? "service" : response.getName();

		return ServiceError.builder().transactionId(errorType.getTransactionid()).code(errorType.getCode())
				.serviceDescription(errorType.getDescription())
				.translatedDescription(translateErrorCode(serviceOperation, errorType.getTransactionid(),
						errorType.getCode(), errorType.getDescription(), errorType.getData()))
				.serviceCallTimestamp(System.currentTimeMillis()).build();
	}

	/**
	 * 
	 * @param operation
	 * @param responseMsg
	 * @return
	 * @throws JAXBException
	 */
	public static ServiceError buildDMSServiceErrorTO(final String responseMsg) {
		dk.semlerit.autocore.ws.client.model.dmsbb.Message message = unmarshal(responseMsg,
				dk.semlerit.autocore.ws.client.model.dmsbb.Message.class);
		dk.semlerit.autocore.ws.client.model.dmsbb.Message.Exception exception = message.getException();
		dk.semlerit.autocore.ws.client.model.dmsbb.Message.Exception.Error error = exception.getError();

		final String serviceOperation = StringUtils.isEmpty(error.getCode().getSrc()) ? "service"
				: error.getCode().getSrc();

		return ServiceError.builder().transactionId(error.getIdrefs()).code(error.getCode().getValue())
				.serviceDescription(error.getText().getValue())
				.translatedDescription(translateErrorCode(serviceOperation, error.getIdrefs(),
						error.getCode().getValue(), error.getText().getValue(), new ArrayList<ErrorDataType>()))
				.serviceCallTimestamp(System.currentTimeMillis()).build();
	}

	/**
	 * 
	 * @param operation
	 * @param e
	 * @return
	 */
	public static ServiceError buildServiceErrorTO(Exception e) {
		String transactionId = null;
		String message = null;
		String code = null;

		if (e instanceof WebServicesFault) {
			// Try to get the transaction Id out from the fault element
			final WebServicesFault fault = (WebServicesFault) e;
			final Element[] elements = fault.getFaultDetails();
			if (Objects.nonNull(elements)) {
				for (final Element element : elements) {
					if (Objects.equals("transactionId", element.getTagName())) {
						transactionId = element.getFirstChild().getNodeValue();
						break;
					}
				}
			}

			message = String.valueOf(e);
			code = Objects.isNull(fault.getFaultCode()) ? null : String.valueOf(fault.getFaultCode());
		} else {
			String m = StringUtils.trimToNull(e.getMessage());
			String t = StringUtils.trimToNull(e.toString());

			if (StringUtils.isNotEmpty(m)) {
				message = m;
			} else if (StringUtils.isNotEmpty(t)) {
				if (Objects.isNull(m)) {
					message = t;
				} else {
					message += " ; " + t;
				}
			}
		}

		return ServiceError.builder().transactionId(transactionId).code(code).serviceDescription(message)
				.translatedDescription(
						translateErrorCode("service", transactionId, code, message, new ArrayList<ErrorDataType>()))
				.serviceCallTimestamp(System.currentTimeMillis()).build();
	}

	/**
	 * TODO needs hardening.
	 * 
	 * @param message
	 * @return
	 */
	public static String resolveOperation(final String message) {
		Pattern pattern = Pattern.compile("NAME=\"(.*?)\"");
		Matcher matcher = pattern.matcher(message);
		if (matcher.find()) {
			return matcher.group(1);
		}
		return "service";
	}

	public static String translateErrorCode(final String operation, String transactionId, final String errorCode,
			final String errorDescription, List<dk.semlerit.autocore.ws.client.model.xml.ErrorDataType> errorData) {
		String result = null;

		Map<String, String> errorParameters = new HashMap<String, String>();
		if (Objects.isNull(errorData) || errorData.isEmpty()) {
			errorParameters.putAll(extractErrorKeyValues(errorDescription));
		} else {
			errorData.forEach(errorDataType -> {
				if (StringUtils.isNotEmpty(errorDataType.getId())) {
					String content = errorDataType.getContent().stream().map(c -> String.valueOf(c))
							.collect(Collectors.joining());
					errorParameters.put(errorDataType.getId(), content);
				}
			});

		}

		if (Objects.isNull(transactionId)) {
			transactionId = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd-HH.mm.ss"));
		}
		errorParameters.put("TRANSACTIONID", transactionId);

		String key = (operation + (StringUtils.isNotEmpty(errorCode) ? "." + errorCode : "")).toLowerCase();

		while (Objects.isNull(result) && StringUtils.isNotEmpty(key)) {
			try {
				result = messages.getString(key);
			} catch (MissingResourceException ignoreException) {
				// Ignore exception but reduce scope for key
				key = StringUtils.substring(key, 0, StringUtils.lastIndexOf(key, "."));
			}
		}

		if (Objects.isNull(result)) {
			return String.format(METHOD_TRANSLATION_NOT_FOUND, transactionId, errorDescription);
		}

		log.debug("Service error occured. TransactionId: {} - errorCode: {} - errorDescription: {}",
				new Object[] { transactionId, errorCode, errorDescription });
		return replaceErrorKeyValues(result, errorParameters);
	}

	private static HashMap<String, String> extractErrorKeyValues(final String str) {
		HashMap<String, String> map = new HashMap<String, String>();
		if (StringUtils.isBlank(str)) {
			return map;
		}
		Matcher m = ERROR_MESSAGE_PATTERN.matcher(str);
		while (m.find()) {
			String key = m.group(1);
			String value = m.group(2);
			map.put(key, value);
		}
		return map;
	}

	private static String replaceErrorKeyValues(String message, Map<String, String> map) {
		if (Objects.isNull(map) || map.isEmpty()) {
			return message;
		} else {
			Matcher m = MESSAGE_REPLACE_PATTERN.matcher(message);

			while (m.find()) {
				String token = m.group();
				// the pattern enforces that the string consists of '{' at least
				// one non '}' char and finally a '}' char, so the substring
				// should be safe
				String key = StringUtils.substring(token, 1, token.length() - 1);
				String value = map.get(key);
				if (value != null) {
					message = message.replaceAll(Pattern.quote(token), value);
				} else {
					// log a warning about the missing parameter
					log.warn("Replace pattern {} found in error message but not in message parameters", token);
				}
			}
			return message;
		}
	}

	@SuppressWarnings("unchecked")
	private static <T> T unmarshal(final String source, final Class<T> responseClass) throws DataBindingException {
		try {
			final JAXBContext ctx = JAXBContext.newInstance(responseClass);
			final Unmarshaller u = ctx.createUnmarshaller();

			return (T) u.unmarshal(new StringReader(source));
		} catch (JAXBException e) {
			throw new DataBindingException(e.getMessage(), e);
		}
	}

	/*
	 * Support for multiple property files collected as a message bundle
	 */
	private static class ServiceCodeMessageBundle extends MultiplePropertiesResourceBundle {
		protected ServiceCodeMessageBundle(String packageName, String baseName) {
			super(packageName, baseName);
		}
	}
}

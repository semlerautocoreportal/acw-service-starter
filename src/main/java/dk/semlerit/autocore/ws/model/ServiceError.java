package dk.semlerit.autocore.ws.model;

import java.io.Serializable;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 
 * 
 * @author edbbrpe
 * @version 1.0
 * @since 3.0.0
 */
@Getter
@Setter
@ToString
@Builder
public class ServiceError implements Serializable {
	private static final long serialVersionUID = -1328837785051511271L;
	private String transactionId;
	private String code;
	private String serviceDescription;
	private String translatedDescription;
	private long serviceCallTimestamp;

}

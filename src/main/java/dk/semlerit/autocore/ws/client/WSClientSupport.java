package dk.semlerit.autocore.ws.client;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URL;
import java.util.Objects;

import javax.xml.bind.DataBindingException;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.util.JAXBSource;
import javax.xml.namespace.QName;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.ws.BindingProvider;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.StopWatch;

import com.ibm.ws.webservices.engine.WebServicesFault;

import dk.semlerit.autocore.exception.ServiceClientException;
import dk.semlerit.autocore.service.client.xmlclient.ExecuteServiceRequest;
import dk.semlerit.autocore.service.client.xmlclient.XMLService;
import dk.semlerit.autocore.service.client.xmlclient.XMLService200903;
import dk.semlerit.autocore.ws.model.ServiceError;
import dk.semlerit.autocore.ws.support.FaultHandlerSupport;
import lombok.extern.slf4j.Slf4j;

/**
 * General WS Client Support class for XML Service clients.
 * 
 * @author edbbrpe
 * @version 1.0
 * @since 3.0.0
 */
@Slf4j
public class WSClientSupport {

	private Unmarshaller unmarshaller;
	private URI endpointURI;
	private String consumerId;
	private FaultResolver faultResolver = new SimpleFaultResolver();
	private final TransformerFactory transformerFactory = TransformerFactory.newInstance();

	/**
	 * Create a new WebService Client with a predefined unmarshaller based on
	 * one or more classes.
	 * 
	 * @param consumerId
	 * @param classesToBeBound
	 */
	public WSClientSupport(String consumerId, Class<?>... classesToBeBound) {
		this.consumerId = Objects.requireNonNull(consumerId, "consumerId must not be null");
		Objects.requireNonNull(classesToBeBound, "JAXB classes must not be null");

		try {
			final JAXBContext context = JAXBContext.newInstance(classesToBeBound);
			this.unmarshaller = context.createUnmarshaller();
		} catch (JAXBException e) {
			throw new ServiceClientException("Could not bind unmarshaller to web client", e);
		}
	}

	/**
	 * Create a new WebService Client with a predefined unmarshaller based on
	 * one or more classes.
	 * 
	 * @param consumerId
	 * @param endpointURI
	 * @param classesToBeBound
	 */
	public WSClientSupport(String consumerId, URI endpointURI, Class<?>... classesToBeBound) {
		this(consumerId, classesToBeBound);

		Objects.requireNonNull(endpointURI, "endpointURI must not be null");
		this.endpointURI = endpointURI;
	}

	/**
	 * Override the default fault resolver.
	 * 
	 * @param faultResolver
	 * @throws NullPointerException
	 *             if faultResolver is null
	 */
	public void setFaultMessageResolver(FaultResolver faultResolver) {
		Objects.requireNonNull(faultResolver, "faultResolver must not be null");

		this.faultResolver = faultResolver;
	}

	// ~ WebServiceOperations methods ==========================================

	/**
	 * 
	 * @param requestPayload
	 * @param consumerId
	 * @return
	 * @throws ServiceClientException
	 */
	public Object marshalSendAndReceive(Object requestPayload) throws ServiceClientException {
		return marshalSendAndReceive(createSource(requestPayload));
	}

	/**
	 * 
	 * @param requestPayload
	 * @param consumerId
	 * @return
	 * @throws ServiceClientException
	 */
	public Object marshalSendAndReceive(Source requestPayload) throws ServiceClientException {
		return marshalSendAndReceive(WSClientSupport.this.endpointURI, requestPayload);
	}

	/**
	 * 
	 * @param uri
	 * @param requestPayload
	 * @param consumerId
	 * @return
	 * @throws ServiceClientException
	 */
	public Object marshalSendAndReceive(String uri, Object requestPayload) throws ServiceClientException {
		return marshalSendAndReceive(uri, createSource(requestPayload));
	}

	/**
	 * 
	 * @param uri
	 * @param requestPayload
	 * @param consumerId
	 * @return
	 * @throws ServiceClientException
	 */
	public Object marshalSendAndReceive(String uri, Source requestPayload) throws ServiceClientException {
		return marshalSendAndReceive(URI.create(uri), requestPayload);
	}

	/**
	 * 
	 * @param uri
	 * @param requestPayload
	 * @param consumerId
	 * @return
	 * @throws ServiceClientException
	 */
	public Object marshalSendAndReceive(URI uri, Object requestPayload) throws ServiceClientException {
		return marshalSendAndReceive(uri, createSource(requestPayload));
	}

	/**
	 * 
	 * @param uri
	 * @param requestPayload
	 * @param consumerId
	 * @return
	 * @throws ServiceClientException
	 */
	public Object marshalSendAndReceive(URI uri, Source requestPayload) throws ServiceClientException {
		if (Objects.isNull(this.unmarshaller)) {
			throw new IllegalStateException("No unmarshaller registered. Check configuration of WebServiceClients.");
		}

		final StopWatch stopWatch = new StopWatch();
		stopWatch.start();
		try {

			final String responseMsg = executeOperation(uri, fromSource(requestPayload));
			faultResolver.resolveFault(responseMsg);

			final Object obj = unmarshaller.unmarshal(new StringReader(responseMsg));
			stopWatch.stop();
			log.debug("Service operation took: {} milliseconds", stopWatch.getTime());

			return obj;

		} catch (IOException | JAXBException e) {
			throw new ServiceClientException("Could not process request", e);
		}
	}

	/**
	 * 
	 * @param requestPayload
	 * @param responseExtractor
	 * @param consumerId
	 * @return
	 * @throws ServiceClientException
	 */
	public <T> T marshalSendAndReceive(Object requestPayload, ResponseExtractor<T> responseExtractor, String consumerId)
			throws ServiceClientException {
		return marshalSendAndReceive(createSource(requestPayload), responseExtractor, consumerId);
	}

	/**
	 * 
	 * @param requestPayload
	 * @param responseExtractor
	 * @param consumerId
	 * @return
	 * @throws ServiceClientException
	 */
	public <T> T marshalSendAndReceive(Source requestPayload, ResponseExtractor<T> responseExtractor)
			throws ServiceClientException {
		return marshalSendAndReceive(WSClientSupport.this.endpointURI, requestPayload, responseExtractor);
	}

	/**
	 * 
	 * @param uri
	 * @param requestPayload
	 * @param responseExtractor
	 * @param consumerId
	 * @return
	 * @throws ServiceClientException
	 */
	public <T> T marshalSendAndReceive(String uri, Object requestPayload, ResponseExtractor<T> responseExtractor)
			throws ServiceClientException {
		return marshalSendAndReceive(URI.create(uri), createSource(requestPayload), responseExtractor);
	}

	/**
	 * 
	 * @param uri
	 * @param requestPayload
	 * @param responseExtractor
	 * @param consumerId
	 * @return
	 * @throws ServiceClientException
	 */
	public <T> T marshalSendAndReceive(String uri, Source requestPayload, ResponseExtractor<T> responseExtractor)
			throws ServiceClientException {
		return marshalSendAndReceive(URI.create(uri), requestPayload, responseExtractor);
	}

	/**
	 * 
	 * @param uri
	 * @param requestPayload
	 * @param responseExtractor
	 * @param consumerId
	 * @return
	 * @throws ServiceClientException
	 */
	public <T> T marshalSendAndReceive(URI uri, Object requestPayload, ResponseExtractor<T> responseExtractor)
			throws ServiceClientException {
		return marshalSendAndReceive(uri, createSource(requestPayload), responseExtractor);
	}

	/**
	 * 
	 * @param uri
	 * @param requestPayload
	 * @param responseExtractor
	 * @param consumerId
	 * @return
	 * @throws ServiceClientException
	 */
	public <T> T marshalSendAndReceive(URI uri, Source requestPayload, ResponseExtractor<T> responseExtractor)
			throws ServiceClientException {
		final StopWatch stopWatch = new StopWatch();
		stopWatch.start();
		try {

			final String responseMsg = executeOperation(uri, fromSource(requestPayload));
			faultResolver.resolveFault(responseMsg);

			T response = responseExtractor.extractData(responseMsg);
			stopWatch.stop();
			log.debug("Service operation took: {} milliseconds", stopWatch.getTime());

			return response;

		} catch (IOException | TransformerException e) {
			throw new ServiceClientException("Could not process request", e);
		}
	}

	/**
	 * 
	 * @param requestPayload
	 * @param responseCallback
	 * @param consumerId
	 * @return
	 * @throws ServiceClientException
	 */
	public boolean marshalSendAndReceive(Object requestPayload, ResponseCallback responseCallback)
			throws ServiceClientException {
		return marshalSendAndReceive(WSClientSupport.this.endpointURI, createSource(requestPayload), responseCallback);
	}

	/**
	 * 
	 * @param uri
	 * @param requestPayload
	 * @param responseCallback
	 * @param consumerId
	 * @return
	 * @throws ServiceClientException
	 */
	public boolean marshalSendAndReceive(String uri, Object requestPayload, ResponseCallback responseCallback)
			throws ServiceClientException {
		return marshalSendAndReceive(URI.create(uri), createSource(requestPayload), responseCallback);
	}

	/**
	 * 
	 * @param uri
	 * @param requestPayload
	 * @param responseCallback
	 * @param consumerId
	 * @return
	 * @throws ServiceClientException
	 */
	public boolean marshalSendAndReceive(String uri, Source requestPayload, ResponseCallback responseCallback)
			throws ServiceClientException {
		return marshalSendAndReceive(URI.create(uri), requestPayload, responseCallback);
	}

	/**
	 * 
	 * @param uri
	 * @param requestPayload
	 * @param responseCallback
	 * @param consumerId
	 * @return
	 * @throws ServiceClientException
	 */
	public boolean marshalSendAndReceive(URI uri, Object requestPayload, ResponseCallback responseCallback)
			throws ServiceClientException {
		return marshalSendAndReceive(uri, requestPayload, responseCallback);
	}

	/**
	 * 
	 * @param uri
	 * @param requestPayload
	 * @param responseCallback
	 * @return
	 * @throws ServiceClientException
	 */
	public boolean marshalSendAndReceive(URI uri, Source requestPayload, ResponseCallback responseCallback)
			throws ServiceClientException {
		final StopWatch stopWatch = new StopWatch();

		try {
			stopWatch.start();

			final String responseMsg = executeOperation(uri, fromSource(requestPayload));
			if (StringUtils.isAllEmpty(StringUtils.trimToNull(responseMsg))) {
				return false;
			}

			faultResolver.resolveFault(responseMsg);

			responseCallback.doWithResponse(responseMsg);
			stopWatch.stop();

			log.debug("Service operation took: {} milliseconds", stopWatch.getTime());

			return true;
		} catch (IOException | TransformerException e) {
			log.error("Service operation failed.", e);
			return false;
		}
	}

	// ~ Private internal Methods & Classes ===================================

	/*
	 * Create a JAXBSource object from a given object. If object is null, a
	 * NullpointerException will be thrown
	 */
	private JAXBSource createSource(Object object) {
		Objects.requireNonNull(object, "object must not be null.");
		try {
			final JAXBContext ctx = JAXBContext.newInstance(object.getClass());
			return new JAXBSource(ctx, object);
		} catch (JAXBException e) {
			throw new DataBindingException(String.format("Could not create JAXB source: %s", String.valueOf(object)),
					e);
		}
	}

	/*
	 * Convert a given source object to a String value. If source object is
	 * null, a NullpointerException will be thrown
	 */
	private String fromSource(Source source) {
		Objects.requireNonNull(source, "source must not be null");
		try {
			Transformer transformer = this.transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			transformer.transform(source, new StreamResult(out));
			return out.toString("UTF-8");
		} catch (IllegalArgumentException | TransformerException | UnsupportedEncodingException e) {
			throw new DataBindingException("Could not create string from JAXB source.", e);

		}
	}

	/*
	 * Execute the actual SOAP call to the ESB infrastructure Service back-end.
	 */
	private String executeOperation(URI uri, String message) throws IOException, WebServicesFault {
		final XMLService200903 service = new XMLService200903(getWSDLLocation(),
				new QName("http://www.semlernet.dk/xmlns/xmlservice/200903/", "XMLService200903"));
		final XMLService xmlService;
		xmlService = service.getXMLService200903SOAP();
		((BindingProvider) xmlService).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY,
				String.valueOf(uri));
		final ExecuteServiceRequest wsRequest = new ExecuteServiceRequest();
		wsRequest.setConsumerId(consumerId);
		wsRequest.setInputMessage(message);

		final String responseMsg = xmlService.executeService(wsRequest).getOutputMessage();

		return responseMsg;
	}

	/*
	 * Retrieve the WSDL Location from as a system resource.
	 */
	private URL getWSDLLocation() {
		return Thread.currentThread().getContextClassLoader().getResource("wsdl/XMLService200903.wsdl");
	}

	/**
	 * Simple fault resolver which will just throw a
	 * {@code ServiceClientException} which contains the resolved ServiceError
	 * detail object.
	 * 
	 * @author edbbrpe
	 * @version 1.0
	 * @since 3.0.0
	 * @see FaultResolver
	 */
	private static class SimpleFaultResolver implements FaultResolver {
		@SuppressWarnings("unchecked")
		@Override
		public <T> T resolveFault(Object responsePayload) throws IOException {
			Objects.requireNonNull(responsePayload, "Response payload must not be null.");

			final ServiceError serviceError;

			if (responsePayload instanceof String) {
				serviceError = handleMessage((String) responsePayload);
			} else if (responsePayload instanceof Exception) {
				serviceError = handleMessage((Exception) responsePayload);
			} else {
				throw new IllegalArgumentException("Message must either be a string or some type of exception.");
			}
			if (Objects.nonNull(serviceError)) {
				throw new ServiceClientException(serviceError.getServiceDescription(), serviceError);
			}
			return ((T) responsePayload);
		}

		/*
		 * Handle message in form of an exception. Typically SOAP message
		 * faults.
		 */
		private ServiceError handleMessage(Exception exception) {
			return FaultHandlerSupport.buildServiceErrorTO(exception);
		}

		/*
		 * Handle messages in form of an XML or DMSBB messages formats.
		 */
		private ServiceError handleMessage(String message) {
			if (StringUtils.contains(message, "<ERROR>")) {
				return FaultHandlerSupport.buildXMLServiceErrorTO(message);
			} else if (StringUtils.contains(message, "<EXCEPTION>")) {
				return FaultHandlerSupport.buildDMSServiceErrorTO(message);
			} else {
				// No error detected according to Semler standards.
				return null;
			}
		}
	}
}

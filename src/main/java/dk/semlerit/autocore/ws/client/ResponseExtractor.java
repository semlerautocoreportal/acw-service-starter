package dk.semlerit.autocore.ws.client;

import java.io.IOException;

import javax.xml.transform.TransformerException;

/**
 * 
 * @author edbbrpe
 * @version 1.0
 * @since 3.0.0
 */
public interface ResponseExtractor<T> {

	/**
	 * Process the data in the given {@code WebServiceMessage}, creating a
	 * corresponding result object.
	 *
	 * @param message
	 *            the message to extract data from
	 * @return an arbitrary result object, or {@code null} if none (the
	 *         extractor will typically be stateful in the latter case)
	 * @throws IOException
	 *             in case of I/O errors
	 * @throws TransformerException
	 *             in case of transformation errors
	 */
	T extractData(String message) throws IOException, TransformerException;
}

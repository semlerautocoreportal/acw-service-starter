/**
 * 
 */
package dk.semlerit.autocore.ws.client;

import java.io.IOException;

import javax.xml.transform.TransformerException;

/**
 * 
 * 
 * @author edbbrpe
 * @version 1.0
 * @since 3.0.0
 */
public interface ResponseCallback {
	/**
	 * Execute any number of operations on the supplied {@code message}.
	 *
	 * @param message
	 *            the message
	 * @throws IOException
	 *             in case of I/O errors
	 * @throws TransformerException
	 *             in case of transformation errors
	 */
	void doWithResponse(String response) throws IOException, TransformerException;
}

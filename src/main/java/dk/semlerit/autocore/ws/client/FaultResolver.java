package dk.semlerit.autocore.ws.client;

import java.io.IOException;

/**
 * 
 * 
 * @author edbbrpe
 * @version 1.0
 * @since 3.0.0
 */
public interface FaultResolver {

	/**
	 * 
	 * @param responsePayload
	 * @ T may be null depending on implementation.
	 * @throws IOException
	 *             or some RuntimeException depending on implementation.
	 */
	<T> T resolveFault(Object responsePayload) throws IOException;
}

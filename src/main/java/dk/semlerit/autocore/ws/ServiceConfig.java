package dk.semlerit.autocore.ws;

import java.util.Objects;

import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

import dk.semlerit.autocore.ws.annotation.BaseURI;
import dk.semlerit.autocore.ws.annotation.ConsumerId;
import dk.semlerit.autocore.ws.entity.Property;

/**
 * 
 * @author edbbrpe
 * @version 1.0
 * @since 3.0.0
 */
public class ServiceConfig {

	// ~ Internal Constants ===================================================
	/** Property constant for SOA environment */
	private static final String SOA_ESB_URI = "SOA.ESB.URL";
	/** Property constant for Portal consumer */
	private static final String SOA_AC_CONSUMERID = "SOA.AC.CONSUMERID";

	private String baseURI;
	private String consumerId;

	@Produces
	@PersistenceContext(unitName = "acw-service-starter", type = PersistenceContextType.TRANSACTION)
	private EntityManager em;

	@Produces
	@BaseURI
	public String baseURI() {
		if (Objects.isNull(baseURI)) {
			String configurationProperty = retrieveConfigurationProperty(SOA_ESB_URI);
			this.baseURI = Objects.requireNonNull(configurationProperty,
					String.format("Configuration property: %s must be available.", SOA_ESB_URI));
		}

		return baseURI;
	}

	/**
	 * Retrieve the Portal consumer id.
	 * 
	 * @return Optional containing the consumerId
	 */
	@Produces
	@ConsumerId
	public String getConsumerId() {
		if (Objects.isNull(consumerId)) {
			consumerId = Objects.requireNonNull(retrieveConfigurationProperty(SOA_AC_CONSUMERID),
					String.format("Configuration property: %s must be available.", SOA_AC_CONSUMERID));
		}

		return consumerId;
	}

	// ~ Private internal Methods =============================================

	private String retrieveConfigurationProperty(final String propertyKey) {
		Property property = em.find(Property.class,
				Objects.requireNonNull(propertyKey, "propertyKey must not be null"));

		return Objects.nonNull(property) ? property.getValue() : null;
	}

}

package dk.semlerit.autocore.ws.adapter;

import java.util.Calendar;
import java.util.Date;

import javax.xml.bind.DatatypeConverter;

import org.apache.commons.lang3.time.FastDateFormat;

/**
 * <p>
 * Special Simple date converter which ensures compliance with Main-frame
 * specific length and format for dates. Should only be used for XJB data
 * binding, and only if needed. Otherwise use the standard
 * {@code javax.xml.bind.DatatypeConverter.* }
 * </p>
 * 
 * @author EXTUBO
 * @author EXTBDA
 * @author extthtb
 * @version 1.2
 * @since 2.7.0
 */
public class SimpleDateAdapter {
	private static final FastDateFormat YYYY_MM_DD = FastDateFormat.getInstance("yyyy-MM-dd");

	/**
	 * Convert a string on XML Schema date format to a Calendar. If
	 * <code>null</code> or an string that trims to empty is given
	 * <code>null</code> is returned.
	 * 
	 * @see http://www.w3.org/TR/xmlschema-2/#date
	 */
	public static Calendar parseCalendar(final String dateStr) {
		return parseSchemaCalendar(dateStr);
	}

	/**
	 * Converts a calendar to a string in format yyyy-MM-dd used to print in
	 * XML, when calling services.
	 * 
	 * @param calendar
	 *            the calendar to print.
	 * @return String in format YYYY-MM-DD
	 */
	public static String printCalendar(final Calendar calendar) {
		return YYYY_MM_DD.format(calendar);
	}

	/**
	 * Convert a string on XML Schema date format to a Date. If
	 * <code>null</code> or an string that trims to empty is given
	 * <code>null</code> is returned.
	 * 
	 * @see http://www.w3.org/TR/xmlschema-2/#date
	 */
	public static Date parseDate(final String dateStr) {
		return parseSchemaDate(dateStr);
	}

	/**
	 * Converts a java.util.date to a date string in format yyyy-MM-dd used to
	 * print in XML, when calling services.
	 * 
	 * @param date
	 *            the java.util.date to print.
	 * @return String in format YYYY-MM-DD
	 */
	public static String printDate(final Date date) {
		return YYYY_MM_DD.format(date);
	}

	// ~ private internal methods ==============================================

	private static Calendar parseSchemaCalendar(final String schemaDate) {
		return DatatypeConverter.parseDate(schemaDate);
	}

	private static Date parseSchemaDate(final String schemaDate) {
		return parseSchemaCalendar(schemaDate).getTime();
	}
}

package dk.semlerit.autocore.exception;

import dk.semlerit.autocore.ws.model.ServiceError;
import lombok.Getter;

/**
 * 
 * @author edbbrpe
 * @version 1.0
 * @since 3.0.0
 */
@Getter
public class ServiceException extends RuntimeException {
	private static final long serialVersionUID = 2156241815578150145L;

	private Object[] additionalData;

	public ServiceException(String msg) {
		super(msg);
	}

	public ServiceException(String msg, ServiceError serviceError) {
		super(msg);
	}

	public ServiceException(String msg, Throwable cause) {
		super(msg, cause);
	}

	public ServiceException(String message, Object... additionalData) {
		super(message);
		this.additionalData = additionalData;
	}

	public ServiceException(String message, Throwable cause, Object... additionalData) {
		super(message, cause);
		this.additionalData = additionalData;
	}
}

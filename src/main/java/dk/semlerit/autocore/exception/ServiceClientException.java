package dk.semlerit.autocore.exception;

import java.util.Optional;

import dk.semlerit.autocore.ws.model.ServiceError;
import lombok.Getter;

/**
 * 
 * @author edbbrpe
 * @version 1.0
 * @since 3.0.0
 */
@Getter
public class ServiceClientException extends RuntimeException {
	private static final long serialVersionUID = 2156241815578150145L;
	private Optional<ServiceError> serviceError = Optional.empty();
	private Object[] additionalData;

	public ServiceClientException(String msg) {
		super(msg);
	}

	public ServiceClientException(String msg, ServiceError serviceError) {
		super(msg);
		this.serviceError = Optional.of(serviceError);
	}

	public ServiceClientException(String msg, Throwable cause) {
		super(msg, cause);
	}

	public ServiceClientException(String message, Object... additionalData) {
		super(message);
		this.additionalData = additionalData;
	}

	public ServiceClientException(String message, Throwable cause, Object... additionalData) {
		super(message, cause);
		this.additionalData = additionalData;
	}
}
